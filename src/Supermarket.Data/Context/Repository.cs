using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Supermarket.Core.Base;

namespace Supermarket.Data.Context
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly HumanResourceContext _dbContext;

        public Repository(HumanResourceContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual T GetById(int id)
        {
            return _dbContext.Set<T>().SingleOrDefault(e => e.Id == id);
        }

        public virtual T Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();

            return entity;
        }

        public virtual void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            _dbContext.Set<T>().Update(entity);
            // _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public virtual List<T> FetchAll()
        {
            return _dbContext.Set<T>().ToList();
        }

        public virtual void Save() => _dbContext.SaveChanges();
    }
}