using System.Collections.Generic;
using System.Linq;
using Supermarket.Core.Models;
using Supermarket.Data.Context;
using Microsoft.EntityFrameworkCore;
using Supermarket.Core.Repositories;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Supermarket.Data.Repositories
{
    public class EmployeeRepository : Context.Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(HumanResourceContext dbContext) : base(dbContext)
        {
        }

        public List<Employee> FetchAllEmployees()
        {
            return this._dbContext
                .Employees
                .Include(x => x.Type)
                .ToList();
        }

        public override Employee GetById(int id)
        {
            return _dbContext.Set<Employee>()
                .Include(x => x.Type)
                .SingleOrDefault(e => e.Id == id);
        }

        public override Employee Add(Employee entity)
        {
            _dbContext.Attach(entity.Type);
            return base.Add(entity);
        }

        public void Update(Employee entity, Employee modifications)
        {
            entity.Email = modifications.Email;
            entity.Firstname = modifications.Firstname;
            entity.Password = modifications.Password;
            entity.Surname = modifications.Surname;
            entity.Type = modifications.Type;
            _dbContext.Attach(entity.Type);
            base.Update(entity);
        }

        public Employee Exist(string email, string password)
        {
            return _dbContext.Set<Employee>().Include(x => x.Type)
                .Where(x => x.Email == email && x.Password == password)
                .FirstOrDefault();
        }

        public bool ExistUser(string email)
        {
            return _dbContext.Set<Employee>()
                .Where(x => x.Email == email)
                .Any();
        }
    }
}