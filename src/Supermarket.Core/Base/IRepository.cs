using System.Collections.Generic;
using System.Linq;

namespace Supermarket.Core.Base
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        List<TEntity> FetchAll();
        TEntity GetById(int id);
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Save();
    }
}