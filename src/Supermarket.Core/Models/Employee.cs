using Supermarket.Core.Base;

namespace Supermarket.Core.Models
{
    public class Employee : BaseEntity
    {
        // `id` int(11) NOT NULL AUTO_INCREMENT,
        // `firstname` varchar(100) NOT NULL,
        public string Firstname { get; set; }
        // `surname` varchar(100) NOT NULL,
        public string Surname { get; set; }
        // `email` varchar(100) NOT NULL,
        public string Email { get; set; }
        // `password` varchar(100) DEFAULT NULL,
        public string Password { get; set; }
        // `employeetype` int(11) NOT NULL,
        public virtual EmployeeType Type { get; set; }
    }
}