namespace Supermarket.Core.Models
{
    public class EmployeeType
    {
        public string Id { get; set; }
        
        public string Initials { get; set; }

        public string Description { get; set; }
    }
}