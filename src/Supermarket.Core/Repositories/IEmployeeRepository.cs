using System.Collections.Generic;
using Supermarket.Core.Base;
using Supermarket.Core.Models;

namespace Supermarket.Core.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        List<Employee> FetchAllEmployees();
        void Update(Employee entity, Employee modifications);

        Employee Exist(string email, string password);
        
        bool ExistUser(string email);
    }
}