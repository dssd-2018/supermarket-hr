using AutoMapper;

namespace Supermarket.Api.Profiles
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Core.Models.Employee, ViewModels.Employee>().ReverseMap();
        }
    }
}