using Supermarket.Core.Models;

namespace Supermarket.Api.ViewModels
{
    public class Employee
    {
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public virtual EmployeeType Type { get; set; }
    }
}