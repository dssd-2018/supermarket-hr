namespace Supermarket.Api.ViewModels
{
    public class EmployeeCredentials
    {
        public string Email { get; set; }
        public string Password {get; set;}
    }
}