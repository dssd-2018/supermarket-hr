using Supermarket.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Supermarket.Api.Configurations
{
    public static class ConnectionConfiguration
    {
        public static IServiceCollection AddConnectionProviderInMemory(this IServiceCollection services, IConfiguration configuration)
        {
            // var connection = configuration.GetConnectionString("HumanResourcesDB");
            services.AddDbContext<HumanResourceContext>(options => options.UseInMemoryDatabase("HumanResources"));

            return services;
        }

        public static IServiceCollection AddConnectionProviderMySql(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("HumanResourcesDB");

            services.AddDbContext<HumanResourceContext>(options => options.UseMySql(connection, opt => opt.MigrationsAssembly("Supermarket.Data")));

            return services;
        }
    }
}