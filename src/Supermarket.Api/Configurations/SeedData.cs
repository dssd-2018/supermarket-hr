using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Supermarket.Data.Context;
using Bogus;
using Supermarket.Core.Models;
using System.Collections.Generic;

namespace Supermarket.Api.Configurations
{
    public static class SeedData
    {
        public static void LoadData(this IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<HumanResourceContext>();
            context.Database.EnsureCreated();
            if (!context.Employees.Any())
            {
                var faker = new Faker("es");
                var tiposEmpleados = new String[] { "Empleado", "Recursos Humanos", "Gerente", "Socio" };

                var list = tiposEmpleados.Select(x => new EmployeeType()
                {
                    Id = faker.Random.Uuid().ToString(),
                    Initials = x.Substring(0, 3),
                    Description = x
                });


                var employees = Enumerable.Range(0, 5)
                .Select(x => new Employee()
                {
                    Firstname = faker.Name.FirstName(),
                    Surname = faker.Name.LastName(),
                    Email = string.Format("{0}{1}@example.com", "employee", x),
                    Password = "employee" + x,
                    Type = list.First(l => l.Description.Contains("Empleado")),
                });

                var hr = Enumerable.Range(0, 5)
                .Select(x => new Employee()
                {
                    Firstname = faker.Name.FirstName(),
                    Surname = faker.Name.LastName(),
                    Email = string.Format("{0}{1}@example.com", "human.resources", x),
                    Password = "human.resources" + x,
                    Type = list.First(l => l.Description.Contains("Recursos Humanos")),
                });

                var managers = Enumerable.Range(0, 5)
                .Select(x => new Employee()
                {
                    Firstname = faker.Name.FirstName(),
                    Surname = faker.Name.LastName(),
                    Email = string.Format("{0}{1}@example.com", "manager", x),
                    Password = "manager" + x,
                    Type = list.First(l => l.Description.Contains("Gerente")),
                });

                context.Employees.AddRange(employees.Concat(hr).Concat(managers));
                context.EmployeeTypes.AddRange(list);

                context.SaveChanges();
            }
        }

        // public static void LoadData(this IServiceProvider serviceProvider)
        // {
        //     var context = serviceProvider.GetRequiredService<HumanResourceContext>();
        //     context.Database.EnsureCreated();
        //     if (!context.Employees.Any())
        //     {
        //         var EmployeeTypeFaker = new Faker<EmployeeType>()
        //             .RuleFor(x => x.Description, f => f.Name.JobDescriptor())
        //             .RuleFor(x => x.Initials, f => f.Name.JobType())
        //             .RuleFor(x => x.Id, f => f.Random.Uuid().ToString());

        //         var list = EmployeeTypeFaker.GenerateLazy(10).ToList();

        //         var EmployeeFaker = new Faker<Employee>()
        //             .RuleFor(x => x.Email, f => f.Internet.Email())
        //             .RuleFor(x => x.Firstname, f => f.Name.FirstName())
        //             .RuleFor(x => x.Surname, f => f.Name.LastName())
        //             .RuleFor(x => x.Password, f => f.Internet.Password())
        //             .RuleFor(x => x.Type, f => f.PickRandom(list));

        //         context.Employees.AddRange(EmployeeFaker.GenerateLazy(10));
        //         context.EmployeeTypes.AddRange(list);

        //         context.SaveChanges();
        //     }
        // }
    }
}