using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Supermarket.Api.ViewModels;
using Supermarket.Core.Base;
using Supermarket.Core.Models;
using Supermarket.Core.Repositories;

namespace Supermarket.Api.Controllers
{
    [Route("api/[controller]"), ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeRepository repository;
        private readonly IMapper mapper;

        public EmployeesController(IEmployeeRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(EmployeeCredentials credentials){
            
            if (!this.repository.ExistUser(credentials.Email)) 
            {
                return NotFound();
            } 

            var employee = this.repository.Exist(credentials.Email, credentials.  Password);
            if ( employee == null)
            {
                return Unauthorized();
            }

            return Ok(mapper.Map<Supermarket.Api.ViewModels.Employee>(employee));
        }

        [HttpGet]
        public IActionResult GetAll() => Ok((this.repository
            .FetchAllEmployees()
            .Select(x => mapper.Map<Supermarket.Api.ViewModels.Employee>(x))));

        [HttpGet("{id}", Name = "GetEmployee")]
        public IActionResult GetById(int id) => Ok(mapper.Map<Supermarket.Api.ViewModels.Employee>(this.repository.GetById(id)));

        [HttpPost]
        public IActionResult Create(Core.Models.Employee employee)
        {
            var entity = this.repository.Add(employee);
            return CreatedAtRoute("GetEmployee", new { id = entity.Id }, null);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Core.Models.Employee employee)
        {
            var entity = this.repository.GetById(id);
            if (entity == null) return NotFound();
            this.repository.Update(entity, employee);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var entity = this.repository.GetById(id);
            if (entity == null) return NotFound();
            this.repository.Delete(entity);
            return NoContent();
        }
    }
}