FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY /src/*.sln ./
COPY /src/Supermarket.Api/*.csproj ./Supermarket.Api/
COPY /src/Supermarket.Core/*.csproj ./Supermarket.Core/
COPY /src/Supermarket.Data/*.csproj ./Supermarket.Data/
RUN dotnet restore

# Copy everything else and build
COPY /src/ ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/Supermarket.Api/out .

COPY entrypoint.sh entrypoint.sh
RUN chmod +x ./entrypoint.sh
CMD /bin/bash ./entrypoint.sh

ENTRYPOINT ["dotnet", "Supermarket.Api.dll"]
